﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using RestReserv.Domain;
using RestReserv.Models.Comment;
using RestReserv.Models.Restaurant;
using RestReserv.Repository.Contracts;

namespace RestReserv.Controllers
{
    public class CommentController : Controller
    {
        private readonly ICommentRepository commentRepository;
        private readonly IRestaurantRepository restaurantRepository;

        public CommentController(ICommentRepository commentRepository, IRestaurantRepository restaurantRepository)
        {
            this.commentRepository = commentRepository;
            this.restaurantRepository = restaurantRepository;
        }

        [HttpGet]
        public IActionResult AddComment(int restaurantId)
        {
            var restaurnt = restaurantRepository.GetById(restaurantId);

            ViewBag.Restaurnt = restaurnt.Adapt<RestaurantModel>();
            return View();
        }

        public async Task<IActionResult> AddComment(CommentCreateModel commentCreateModel)
        {
            var restaurnt = restaurantRepository.GetById(commentCreateModel.RestaurantId);

            var comment = new Comment
            {
                Body = commentCreateModel.Body,
                Date = DateTime.Now,
                RestaurantId = restaurnt.Id,
                User = commentCreateModel.User
            };

            await commentRepository.Create(comment);

            return RedirectToAction("Index", "Rest");

            //return View();
        }
    }
}