﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using RestReserv.Domain;
using RestReserv.Models.Order;
using RestReserv.Models.Restaurant;
using RestReserv.Repository.Contracts;

namespace RestReserv.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderRepository orderRepository;
        private readonly IRestaurantRepository restaurantRepository;
        private readonly ITableRepository tableRepository;

        public OrderController(IOrderRepository orderRepository, ITableRepository tableRepository, IRestaurantRepository restaurantRepository)
        {
            this.orderRepository = orderRepository;
            this.tableRepository = tableRepository;
            this.restaurantRepository = restaurantRepository;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var orders = orderRepository.GetAll()?
                .Adapt<IEnumerable<OrderModel>>();

            ViewBag.Orders = orders;

            return View();
        }

        [HttpGet]
        public IActionResult Detail(int id)
        {
            var order = orderRepository.GetById(id);
            if (order == null)
                throw new Exception("Заказ не найден!");
            var model = order.Adapt<OrderModel>();
            model.Address = order.Table.Restaurant.Address;

            ViewBag.Order = model;

            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            var restaurants = restaurantRepository.GetAll()?
                .Select(x => new RestaurantModel
                {
                    Id = x.Id,
                    Address = x.Address,
                    Title = x.Title,
                    City = x.City.Name
                });

            ViewBag.Restaurants = restaurants;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(OrderCreateModel createModel)
        {
            try
            {
                var restaurant = restaurantRepository.GetAll()?.FirstOrDefault(x => x.Id == createModel.RestaurantId);
                if (restaurant == null)
                    throw new Exception("Адрес ресторана указан не верно!");
                if (string.IsNullOrWhiteSpace(createModel.UserName))
                    throw new Exception("Имя заказчика не может быть пустым");

                var table = restaurant.Tables.FirstOrDefault(x => x.CountPlace >= createModel.CountPerson
                && (x.Orders == null || !x.Orders.Any(v => v.Date == createModel.Date) || !x.Orders.Any(v=>v.Time == createModel.Time)));

                if (table == null)
                    throw new Exception("В данном заведении нет свободных мест на данное время!");

                var order = new Order
                {
                    CountPerson = createModel.CountPerson,
                    Date = createModel.Date,
                    Phone = createModel.Phone,
                    Time = createModel.Time,
                    UserName = createModel.UserName,
                    Table = table
                };

                await orderRepository.Create(order);

                return RedirectToAction("Detail", new { id = order.Id });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", ex.Message);
            }
            //return View();

        }

        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var order = orderRepository.GetById(id);
                if (order == null)
                    throw new Exception("Заказ не найден!");

                if (order.Date <= DateTime.Now
                    && order.Time < DateTime.Now.TimeOfDay)
                    throw new Exception("Невозможно удалить данный заказ!");

                await orderRepository.Delete(order);
                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", ex.Message);
            }
        }

        public IActionResult Error(string error)
        {
            ViewBag.Error = error;
            return View();
        }
    }
}