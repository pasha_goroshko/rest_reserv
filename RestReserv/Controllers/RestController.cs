﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using RestReserv.Models.Comment;
using RestReserv.Models.Restaurant;
using RestReserv.Repository.Contracts;

namespace RestReserv.Controllers
{
    public class RestController : Controller
    {
        private readonly ICityRepository cityRepository;
        private readonly IRestaurantRepository restaurantRepository;
        private readonly ICommentRepository commentRepository;

        public RestController(ICityRepository cityRepository,
                              IRestaurantRepository restaurantRepository,
                              ICommentRepository commentRepository)
        {
            this.restaurantRepository = restaurantRepository;
            this.cityRepository = cityRepository;
            this.commentRepository = commentRepository;
        }

        public async Task<IActionResult> Index()
        {
            var restaurants = restaurantRepository.GetAll()?
                .Select(x => new RestaurantModel
                {
                    Id = x.Id,
                    Address = x.Address,
                    Title = x.Title,
                    City = x.City.Name
                });
            var comments = commentRepository.GetAll()?.OrderByDescending(x => x.Id).Take(5).Adapt<IEnumerable<CommentModel>>();

            ViewBag.Restaurants = restaurants;
            ViewBag.Comments = comments;

            return View();
        }
    }
}