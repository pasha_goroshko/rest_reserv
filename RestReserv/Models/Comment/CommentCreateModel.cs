﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestReserv.Models.Comment
{
    public class CommentCreateModel
    {
        /// <summary>
        /// Тело комментария
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Наименование пользователя, оставившего комментарий
        /// </summary>
        public string User { get; set; }

        public int RestaurantId { get; set; }
    }
}
