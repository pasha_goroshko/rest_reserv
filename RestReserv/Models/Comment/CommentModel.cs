﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestReserv.Models.Comment
{
    public class CommentModel
    {
        public int Id { get; set; }

        /// <summary>
        /// Тело комментария
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Наименование пользователя, оставившего комментарий
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Дата комментария
        /// </summary>
        public DateTime Date { get; set; }

        public int RestaurantId { get; set; }
    }
}
