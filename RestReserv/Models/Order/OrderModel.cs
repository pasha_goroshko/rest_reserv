﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RestReserv.Models.Order
{
    public class OrderModel
    {
        public int Id { get; set; }
        /// <summary>
        /// Дата заказа
        /// </summary>
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        /// <summary>
        /// Время заказа
        /// </summary>
        public TimeSpan Time { get; set; }
        /// <summary>
        /// Количество персон
        /// </summary>
        public int CountPerson { get; set; }
        /// <summary>
        /// Имя заказчика
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Телефон заказчика
        /// </summary>
        public string Phone { get; set; }

        public string Address { get; set; }
    }
}
