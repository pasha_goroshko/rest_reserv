﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestReserv.Models.Restaurant
{
    public class RestaurantModel
    {
        public int Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Адрес ресторана
        /// </summary>
        public string Address { get; set; }

        public string City { get; set; }
    }
}
