﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace RestReserv.Migrations
{
    public partial class InitialDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Restaurant",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    CityId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Restaurant", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Restaurant_Cities_CityId",
                        column: x => x.CityId,
                        principalTable: "Cities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Body = table.Column<string>(nullable: true),
                    User = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    RestaurantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comment_Restaurant_RestaurantId",
                        column: x => x.RestaurantId,
                        principalTable: "Restaurant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Table",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CountPlace = table.Column<int>(nullable: false),
                    RestaurantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Table", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Table_Restaurant_RestaurantId",
                        column: x => x.RestaurantId,
                        principalTable: "Restaurant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Order",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Date = table.Column<DateTime>(nullable: false),
                    Time = table.Column<TimeSpan>(nullable: false),
                    CountPerson = table.Column<int>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    TableId = table.Column<int>(nullable: false),
                    RestaurantId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Order_Restaurant_RestaurantId",
                        column: x => x.RestaurantId,
                        principalTable: "Restaurant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order_Table_TableId",
                        column: x => x.TableId,
                        principalTable: "Table",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comment_RestaurantId",
                table: "Comment",
                column: "RestaurantId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_RestaurantId",
                table: "Order",
                column: "RestaurantId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_TableId",
                table: "Order",
                column: "TableId");

            migrationBuilder.CreateIndex(
                name: "IX_Restaurant_CityId",
                table: "Restaurant",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_Table_RestaurantId",
                table: "Table",
                column: "RestaurantId");

            migrationBuilder.Sql("INSERT INTO \"Cities\"(\"Name\") VALUES ('Москва');");
            migrationBuilder.Sql("INSERT INTO \"Cities\"(\"Name\") VALUES ('Санкт-Петербург');");

            migrationBuilder.Sql("INSERT INTO public.\"Restaurant\"(\"Title\", \"Address\", \"CityId\") VALUES ('Ресторан на донской', 'ул. Донская, д. 8', (select \"Id\" from \"Cities\" where \"Name\" = 'Москва'));");
            migrationBuilder.Sql("INSERT INTO public.\"Restaurant\"(\"Title\", \"Address\", \"CityId\") VALUES ('Ресторан на петровке', 'ул. Петровка, д. 45', (select \"Id\" from \"Cities\" where \"Name\" = 'Москва'));");
            migrationBuilder.Sql("INSERT INTO public.\"Restaurant\"(\"Title\", \"Address\", \"CityId\") VALUES ('Ресторан на садовом', 'ул. Большая садовая, д. 34', (select \"Id\" from \"Cities\" where \"Name\" = 'Москва'));");
            migrationBuilder.Sql("INSERT INTO public.\"Restaurant\"(\"Title\", \"Address\", \"CityId\") VALUES ('Ресторан на тверской', 'ул. Тверская, д. 15', (select \"Id\" from \"Cities\" where \"Name\" = 'Москва'));");
            migrationBuilder.Sql("INSERT INTO public.\"Restaurant\"(\"Title\", \"Address\", \"CityId\") VALUES ('Ресторан на никольской', 'ул. Никольская, д. 21', (select \"Id\" from \"Cities\" where \"Name\" = 'Москва'));");

            migrationBuilder.Sql("INSERT INTO public.\"Restaurant\"(\"Title\", \"Address\", \"CityId\") VALUES ('Ресторан на амбарной', 'ул. Амбарная, д. 10', (select \"Id\" from \"Cities\" where \"Name\" = 'Санкт-Петербург'));");
            migrationBuilder.Sql("INSERT INTO public.\"Restaurant\"(\"Title\", \"Address\", \"CityId\") VALUES ('Ресторан на атаманской', 'ул. Атаманская, д. 18', (select \"Id\" from \"Cities\" where \"Name\" = 'Санкт-Петербург'));");
            migrationBuilder.Sql("INSERT INTO public.\"Restaurant\"(\"Title\", \"Address\", \"CityId\") VALUES ('Ресторан на гагаринской', 'ул. Гагаринская, д. 5', (select \"Id\" from \"Cities\" where \"Name\" = 'Санкт-Петербург'));");

            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на донской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на донской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на донской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (5 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на донской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (10 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на донской'));");

            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на петровке'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на петровке'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на петровке'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (5 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на петровке'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (10 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на петровке'));");

            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на садовом'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на садовом'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на садовом'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (5 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на садовом'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (10 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на садовом'));");

            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на тверской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на тверской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на тверской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (5 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на тверской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (10 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на тверской'));");

            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на никольской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на никольской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на никольской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (5 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на никольской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (10 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на никольской'));");

            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на амбарной'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на амбарной'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на амбарной'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (5 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на амбарной'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (10 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на амбарной'));");

            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на атаманской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на атаманской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на атаманской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (5 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на атаманской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (10 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на атаманской'));");

            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на гагаринской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на гагаринской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (3 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на гагаринской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (5 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на гагаринской'));");
            migrationBuilder.Sql("INSERT INTO \"Table\"(\"CountPlace\", \"RestaurantId\") VALUES (10 ,(SELECT \"Id\"	FROM \"Restaurant\" WHERE \"Title\" = 'Ресторан на гагаринской'));");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comment");

            migrationBuilder.DropTable(
                name: "Order");

            migrationBuilder.DropTable(
                name: "Table");

            migrationBuilder.DropTable(
                name: "Restaurant");

            migrationBuilder.DropTable(
                name: "Cities");
        }
    }
}
