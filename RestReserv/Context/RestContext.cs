﻿using Microsoft.EntityFrameworkCore;
using RestReserv.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestReserv.Context
{
    public class RestContext : DbContext
    {
        public DbSet<City> Cities { get; set; }

        public RestContext(DbContextOptions<RestContext> options)
            :base(options)
        {
            //Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
            //optionsBuilder.UseSqlServer("");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>()
.Property(b => b.Id)
       .UseNpgsqlIdentityColumn()
.ValueGeneratedOnAdd();

            modelBuilder.Entity<Comment>()
       .Property(b => b.Id)
       .UseNpgsqlIdentityColumn()
       .ValueGeneratedOnAdd();

            modelBuilder.Entity<Order>()
       .Property(b => b.Id)
       .UseNpgsqlIdentityColumn()
       .ValueGeneratedOnAdd();

            modelBuilder.Entity<Restaurant>()
       .Property(b => b.Id)
       .UseNpgsqlIdentityColumn()
       .ValueGeneratedOnAdd();

            modelBuilder.Entity<Table>()
       .Property(b => b.Id)
       .UseNpgsqlIdentityColumn()
       .ValueGeneratedOnAdd();

       //     modelBuilder.Entity<City>()
       //.HasData(new City
       //{
       //    Name = "Москва",
       //    Restaurants = new List<Restaurant>
       //    {
       //                    new Restaurant
       //                    {
       //                        Address = "ул. Донская 8",
       //                        Title = "Ресторан на донской"
       //                    }
       //    }
       //});

            base.OnModelCreating(modelBuilder);
        }
    }
}
