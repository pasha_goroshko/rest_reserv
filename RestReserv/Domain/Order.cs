﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestReserv.Domain
{
    public class Order
    {
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// Дата заказа
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Время заказа
        /// </summary>
        public TimeSpan Time { get; set; }
        /// <summary>
        /// Количество персон
        /// </summary>
        public int CountPerson { get; set; }
        /// <summary>
        /// Имя заказчика
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Телефон заказчика
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Столик
        /// </summary>
        public virtual Table Table { get; set; }
        public int TableId { get; set; }
    }
}
