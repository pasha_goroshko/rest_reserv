﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestReserv.Domain
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Тело комментария
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Наименование пользователя, оставившего комментарий
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Дата комментария
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Рестаран, к которому относится данный комментарий
        /// </summary>
        public virtual Restaurant Restaurant { get; set; }
        public int RestaurantId { get; set; }
    }
}
