﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestReserv.Domain
{
    /// <summary>
    /// Столик в рестаране
    /// </summary>
    public class Table
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Количество мест в столике
        /// </summary>
        public int CountPlace { get; set; }

        public virtual Restaurant Restaurant { get; set; }
        public int RestaurantId { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
