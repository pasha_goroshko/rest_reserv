﻿using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestReserv.Domain
{
    /// <summary>
    /// Город
    /// </summary>
    public class City
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Список ресторанов в данном городе
        /// </summary>
        public virtual ICollection<Restaurant> Restaurants { get; set; }
    }
}
