﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestReserv.Domain
{
    /// <summary>
    /// Рестаран
    /// </summary>
    public class Restaurant
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Адрес ресторана
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// город, к которому относится данный рестаран
        /// </summary>
        public virtual City City { get; set; }
        public int CityId { get; set; }

        /// <summary>
        /// Комментарии к данному рестарану
        /// </summary>
        public virtual ICollection<Comment> Comments { get; set; }

        /// <summary>
        /// Список столиков
        /// </summary>
        public virtual ICollection<Table> Tables { get; set; }

        /// <summary>
        /// Список заказов
        /// </summary>
        public virtual ICollection<Order> Orders { get; set; }
    }
}
