﻿using RestReserv.Context;
using RestReserv.Domain;
using RestReserv.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestReserv.Repository.Implementations
{
    public class CityRepository : RepositoryBase<City>, ICityRepository
    {
        public CityRepository(RestContext dbContext)
            : base(dbContext)
        {
        }
    }
}
