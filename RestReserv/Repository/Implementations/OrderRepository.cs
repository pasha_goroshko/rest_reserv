﻿using RestReserv.Context;
using RestReserv.Domain;
using RestReserv.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestReserv.Repository.Implementations
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(RestContext dbContext) : base(dbContext)
        {
        }
    }
}
