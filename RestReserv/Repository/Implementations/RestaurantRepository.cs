﻿using RestReserv.Context;
using RestReserv.Domain;
using RestReserv.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestReserv.Repository.Implementations
{
    public class RestaurantRepository : RepositoryBase<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository(RestContext dbContext)
            : base(dbContext)
        {
        }
    }
}
