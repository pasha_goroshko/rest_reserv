﻿using RestReserv.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestReserv.Repository.Contracts
{
    public interface ICityRepository : IRepository<City>
    {
    }
}
